(function () {
  'use strict';

  angular.module('BlurAdmin.pages.account.login', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('login', {
          url: '/login',
          title: 'login',
          templateUrl: 'app/pages/account/login/login.html',
          controller: 'loginCtrl',
        });
  }

})();
