(function () {
  'use strict';

  angular.module('BlurAdmin.pages.account.register', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('register', {
          url: '/register',
          title: 'register',
          templateUrl: 'app/pages/account/register/register.html',
          controller: 'registerCtrl',
        });
  }

})();
