(function () {
  'use strict';

  angular.module('BlurAdmin.pages.account')
    .controller('registerCtrl', registerCtrl);

  /** @ngInject */
  function registerCtrl($scope, $state) {
	$scope.goLogin = function(){
		$state.go('login');
	}
  }

})();
