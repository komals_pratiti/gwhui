(function () {
  'use strict';

  angular.module('BlurAdmin.pages.account', [
      'BlurAdmin.pages.account.register',
      'BlurAdmin.pages.account.login'
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    /**/
  }

})();
