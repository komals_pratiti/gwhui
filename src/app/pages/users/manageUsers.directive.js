/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.users')
      .directive('manageUsers', manageUsers);

  /** @ngInject */
  function manageUsers() {
    return {
      restrict: 'E',
      templateUrl: 'app/pages/users/manageUsers.html',
      title: 'Registred Users',
      authenticate : true,
      controller:'manageUsersCtrl',
      controllerAs : 'vm',
    };
  }
})();