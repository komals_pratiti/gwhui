/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.users')
      .controller('manageUsersCtrl', manageUsersCtrl);

  /** @ngInject */
  function manageUsersCtrl($scope, $uibModal, baProgressModal,  $localStorage, $http) {

    //$scope.authorities = $localStorage.userAccount.role;
    //console.log($localStorage.userAccount.role);

    var modalInstance = null;
    var closeModal = function () {
            modalInstance.close();
        };

    getUsers();
    function getUsers(){
      $scope.users = [
        {'id':1, 'firstName': 'Admin', 'lastName': 'Super', 'email': 'komal@mail.com', 'activated': true},
        {'id':2, 'firstName': 'Mark', 'lastName': 'Clark', 'email': 'markc@mail.com', 'activated': true},
        {'id':3, 'firstName': 'Garry', 'lastName': 'Kolvem', 'email': 'garryk@mail.com', 'activated': true},
        ];
      console.log($scope.users);
      //$scope.users = AdminUser.get();
    };
   
  	$scope.open = function (url, page, controller, dataitem, size) {
      console.log(dataitem);
      //$scope.authorities = $localStorage.userAccount.role;
      //var authorities = $localStorage.userAccount.role;
      //console.log($localStorage.userAccount.role);
      modalInstance = $uibModal.open({
        animation: true,
        controller: controller,
        templateUrl: url,
        size: size,
        resolve: {
          type: function () {
            return page;
          },
          item: function (){
            return dataitem;
          }
        }
      });
      modalInstance.result.then(
                closeModal,
                closeModal
            );
    };
    $scope.openProgressDialog = baProgressModal.open;
  }

})();
