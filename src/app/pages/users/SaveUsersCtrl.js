/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.users')
      .controller('SaveUsersCtrl', SaveUsersCtrl);
  
  SaveUsersCtrl.$inject = ['$scope', '$http', '$state', 'type', 'item', 'User', '$uibModalInstance', '$localStorage'];
  function SaveUsersCtrl($scope, $http, $state, type, item, User, $uibModalInstance, $localStorage) {
    $scope.ForWhat = type;
    $scope.item = $scope.vm = item;
    console.log($scope.vm);
    console.log($scope.item);
    
    getCustomers();
    function getCustomers(){
      var url = "api/customer/getAll";
      $http({
          url: url,
          method: 'GET',
          headers: { 'Content-Type': 'application/json' }
        }).success(function(response) {
          $scope.customers = response;
          console.log($scope.users);
        }).error(function(response) {
          console.log('in error');
          console.log(response);
        });
    };

    $scope.save = function() {
      console.log("posting data....");
      
      if(typeof item === 'undefined' || !item){

        console.log( $scope.vm.customer);
        User.save({
            id: $scope.vm.id,
            firstName : ($scope.vm.firstName ? $scope.vm.firstName : $scope.item.firstName),
            lastName : ($scope.vm.lastName ? $scope.vm.lastName : $scope.item.lastName),
            email : ($scope.vm.email ? $scope.vm.email : $scope.item.email),
            login: ($scope.vm.email ? $scope.vm.email : $scope.item.email),
            password: ($scope.vm.password ? $scope.vm.password : $scope.item.password),
            customerId: ($scope.vm.customer ? $scope.vm.customer : $localStorage.userAccount.customerId),
            role: ($scope.vm.role ? $scope.vm.role : 'ROLE_USER')
        }, function(){
          console.log("about to refresh page");
          window.location.reload();
        });
      } else {
        User.update({
            id: $scope.vm.id,
            firstName : ($scope.vm.firstName ? $scope.vm.firstName : $scope.item.firstName),
            lastName : ($scope.vm.lastName ? $scope.vm.lastName : $scope.item.lastName),
            email : ($scope.vm.email ? $scope.vm.email : $scope.item.email),
            login: ($scope.vm.email ? $scope.vm.email : $scope.item.email),
            password: ($scope.vm.password ? $scope.vm.password : $scope.item.password),
            customerId: ($scope.vm.customer ? $scope.vm.customer : $scope.item.customer),
            role: ($scope.vm.role ? $scope.vm.role : $scope.item.role)
        }, function(){
          console.log("about to refresh page");
          window.location.reload();
        });
      }   
      $uibModalInstance.close(true);   
    }

  }

})();