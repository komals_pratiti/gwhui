'use strict';

angular.module('BlurAdmin', [
  'ngStorage',
  'ngResource',
  'ngCookies',
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngTouch',
  'toastr',
  'smart-table',
  "xeditable",
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',

  'BlurAdmin.theme',
  'BlurAdmin.pages'
])
.config(function ($httpProvider) {
    //$httpProvider.interceptors.push('authInterceptor');
    $httpProvider.interceptors.push(apiInterceptor);
})
.config(localStorageConfig);

localStorageConfig.$inject = ['$localStorageProvider', '$sessionStorageProvider'];
function localStorageConfig($localStorageProvider, $sessionStorageProvider) {
    $localStorageProvider.setKeyPrefix('jhi-');
    $sessionStorageProvider.setKeyPrefix('jhi-');
}

// api url prefix
var API_URL = 'http://localhost:8080/';

function apiInterceptor ($q) {
  return {
    request: function (config) {
      var url = config.url;
      
      // ignore template requests
      if (url.substr(url.length - 5) == '.html') {
        return config || $q.when(config);
      } 
      
      config.url = API_URL + config.url;
      return config || $q.when(config);
    }
  }
}